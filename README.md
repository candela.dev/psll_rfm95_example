# Plain and Simple LoraWAN Library example project
With the LMIC library, using OTAA, I could not reduce the power enough to run a my
MiniPill LoRa node on a CR2032 for longer time than a month.

This was irritating because even my ATtiny84 node performed better than that.
I was very happy with the ATtiny code, because it was straitforward, as LMIC is
not. LMIC is build with RTOS features is not easy to read and to adapt for your
microcontroller.

## Plain and Simple LoraWan Library (PSLL)
So I looked for clear code and found the work of Beelan, who bases his work on
code from Gerben den Hartog (Ideetron). Many thanks to them.

I did not used pull request, but created my own library and example.

Goals:

1. Understand the code and hence the mechanisme of OTAA and downlink commands.

2. Using OTAA and prevent unnecessary downlinks on TTN, which was bothering me with my
ATtiny node.

3. Implement Low Power to reduce power to the max and have control over the
settings

4. Configurable timing settings for correct timing

## Mixed code conventions
Due to using code build by others, the code conventions are mixed as wel as
choices of variable types.
