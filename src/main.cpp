/**
 * Example and test of OTAA device connection
 * Author: Leo Korbee
 * 2022-10-23
 * Tested on MiniPill LoRa (STM32L051C8T6 with RFM95W)
 * See end of file for connection diagram
 * 
 * Adapted main code with some improvements:
 * - Added DEBUG possibilities
 * - Added security feature/file - do not upload this one to your git-repository!
 * 
 * Adapted PS4LoRa library and this example code for some improvements:
 * - Added ADR return message for 0x06 Request
 * - Improved DevNonce calculation/randomization
 * - Mac command processing
 * - Confirmed downlink works!
 * - Low Power mode, also during Join and Cycle (send and receive RX1 or RX2)
 * - RTC timing correction for Low Power mode
 * 
 * @2022-12-05 final version befor publication
 * @2022-12-11 added Wrapper for RTC callibration arround STM32LowPower
 */

#include "lorawan.h"
// includes for security credentials
#include "secconfig.h"
// include for low power with Calibration
#include "STM32LowPowerCal.h"

// uncomment for debugging main
#define DEBUG_MAIN

#ifdef DEBUG_MAIN
  // for debugging redirect to hardware Serial2
  // Tx on PA2
  #define DEBUG_INIT(...) HardwareSerial Serial2(USART2)   // or HardWareSerial Serial2(PA3, PA2);
  #define DEBUG_BEGIN(...) Serial2.begin(9600)
  #define DEBUG_PRINT(...) Serial2.print(__VA_ARGS__)
  #define DEBUG_PRINTLN(...) Serial2.println(__VA_ARGS__)
  DEBUG_INIT();
#else
  #define DEBUG_BEGIN(...)
  #define DEBUG_PRINT(...)
  #define DEBUG_PRINTLN(...)
#endif

// Sleep this many microseconds. Notice that the sending and waiting for downlink
// will extend the time between send packets. You have to extract this time
#define SLEEP_INTERVAL 60000

// global vars 
// counter, number of send actions
unsigned int counter = 0;
// received data and status
char outStr[255];
byte recvStatus = 0;

// see connection diagram at the end of this file
// MOSI pin is used to get lowPower settings on RFM module
const sRFM_pins RFM_pins =
{
  .MOSI = PA7,
  .CS = PA4,
  .RST = PA9,
  .DIO0 = PA10,
  .DIO1 = PB4,
  .DIO2 = PB5
//  .DIO5 = ,
};

// Get the rtc object
// STM32RTC& rtc = STM32RTC::getInstance();

void setup()
{ 
  // Setup serial debug
  DEBUG_BEGIN();
  // time for serial settings
  delay(1000);

/************************************************
 * get TimeCorrection number for RTC times
 ************************************************/
  // wait 8 seconds to upload new code and
  // calibrate the RTC times with the HSI internal clock
  // time calibration on device with correct timing (ideal 8000.00)
  // incease this time to shorten time between send and receive
  LowPowerCal.setRTCCalibrationTime(7980.0);
  // callibrate for 8 seconds
  LowPowerCal.calibrateRTC();
  DEBUG_PRINT("RTC Time Correction: ");
  DEBUG_PRINTLN(LowPowerCal.getRTCTimeCorrection(), 5);
  
/************************************************
 * LoRa init and set parameters
 ************************************************/
  if(!lora.init())
  {
    DEBUG_PRINTLN("RFM95 not detected");
    delay(5000);
    return;
  }
  // Set LoRaWAN Class change CLASS_A or CLASS_C
  lora.setDeviceClass(CLASS_A);
  // Set Data Rate for join!
  lora.setDataRate(SF7BW125);
  // Set Receive Delay for RX1 window
  lora.setReceiveDelay1(5);
  // set channel to random
  lora.setChannel(MULTI);
  // use this setting for power settings
  // power level between 0 and max 15 0dBm -> 15 dBm?
  lora.setTxPower1(TX_0dBm);
  // Put OTAA Key and DevAddress here from secconfig.h file
  lora.setDevEUI(devEui);
  lora.setAppEUI(appEui);
  lora.setAppKey(appKey);

/************************************************
 * LoRa Start Join Process
 ************************************************/
  bool isJoined = false;
  do
  {
    DEBUG_PRINTLN("Joining...");
    isJoined = lora.join();
    // faster 
    if (isJoined) break;
    
    //wait for 15s to try again
    lora.sleepLowPower();
    LowPower.begin();
    LowPower.deepSleep(15000);

  }while(!isJoined);
  DEBUG_PRINTLN("Joined to network");
}

void loop()
{
/************************************************
 * Set data ready for sending with LoRa
 ************************************************/
  // replace this part by your data collection code
  char myStr[] = "Hello World";
  sprintf(myStr, "Hello World! %d", counter);
  DEBUG_PRINT("Send data: ");
  DEBUG_PRINTLN(myStr);
  DEBUG_PRINT("Frame Counter: ");
  DEBUG_PRINTLN(lora.getFrameCounter());
  lora.setUplinkData(myStr, strlen(myStr), 0, 1);
  counter++;

/************************************************
 * LoRa Send data and wait for received data
 ************************************************/
  // cycle -> send (if new message available) and wait for response in RX1 and RX2
  lora.cycle();

/************************************************
 * Check if data is received and print
 ************************************************/
  recvStatus = lora.readData(outStr);
  if(recvStatus)
  {
    DEBUG_PRINTLN(outStr);
    int i = 0;
    while(outStr[i] != '\0' && i < sizeof(outStr))
    {
        DEBUG_PRINT(outStr[i], HEX);
        DEBUG_PRINT(" ");
        i++;
    }
    DEBUG_PRINTLN();
  }

/************************************************
 * Power down and going to long sleep
 ************************************************/
  DEBUG_PRINTLN("Going to long sleep");
  lora.sleepLowPower();
  LowPowerCal.begin();
  LowPowerCal.deepSleep(SLEEP_INTERVAL, 0);
  
}

/************************************************
 * MiniPill LoRa v1.x mapping - LoRa module RFM95W 
 ************************************************/
/* 
  PA4  // SPI1_NSS   NSS  - RFM95W
  PA5  // SPI1_SCK   SCK  - RFM95W
  PA6  // SPI1_MISO  MISO - RFM95W
  PA7  // SPI1_MOSI  MOSI - RFM95W

  PA10 // USART1_RX  DIO0 - RFM95W
  PB4  //            DIO1 - RFM95W
  PB5  //            DIO2 - RFM95W

  PA9  // USART1_TX  RST  - RFM95W

  VCC - 3V3
  GND - GND
*/
